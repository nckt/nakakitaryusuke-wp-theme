<?php
/*
Template Name: about
*/

/**
 * about.php
 *
 * 自己紹介ページ。
 *
 * @author junsuke nakakita <galikuson@gmail.com>
 */
?>
<html>
  <head prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb# article: http://ogp.me/ns/article#">
    <?php get_template_part('head_elements'); ?>

    <!----
      OGP
    ----->
    <meta property="og:type" content="article" />
    <meta property="og:image" content="<?php bloginfo('template_directory');?>/static/img/ryusukenakakita.png" />
    <meta name="twitter:card" content="summary" />
    <?php
      if ( have_posts() ) : while ( have_posts() ) : the_post();
        // 概要
        $description = strip_tags( get_the_content() );
        echo '<meta property="og:description" content="' . $description . '" />';
      endwhile; endif;
    ?>
  </head>
  <body>
    <!------------------
      PAGE TOP CONTENTS
    -------------------->
    <?php get_template_part('pagetop_contents'); ?>

    <!-------
      HEADER
    --------->
    <header class="header header--about">
    </header>

    <!------------
      MAIN CONTENT
    -------------->
    <div class="about_bg">
      <article class="content content--about">
        <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
          <section class="about">
            <?php the_content(); ?>
          </section>
        <?php endwhile; endif;?>
      </article>

      <!-------
        FOOTER
      --------->
      <?php get_footer(); ?>
    </div>
  </body>
</html>
