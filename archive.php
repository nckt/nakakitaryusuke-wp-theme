<?php
/**
 * archive.php
 *
 * 記事一覧。カテゴリ別やタグ別の表示でも使用。
 *
 * @author junsuke nakakita <galikuson@gmail.com>
 */
?>
<html prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb# article: http://ogp.me/ns/article#">
  <head>
    <?php get_template_part('head_elements'); ?>

    <!----
      OGP
    ----->
    <meta property="og:type" content="article" />
    <meta property="og:image" content="<?php bloginfo('template_directory');?>/static/img/ryusukenakakita.png" />
    <meta property="og:description" content="I'm Ryusuke Nakakita, A Graphic designer based in Tokyo. This website is Mainly a limited display of my projects." />
    <meta name="twitter:card" content="summary" />
    <script type="text/javascript" charset="utf-8" src="<?php bloginfo('template_directory');?>/static/js/archive.js"></script>
  </head>
  <body class="archive_body">
    <!------------------
      PAGE TOP CONTENTS
    -------------------->
    <?php get_template_part('pagetop_contents'); ?>

    <!-------
      HEADER
    --------->
    <header class="header">
      <!----
        NAV
      ------>
      <?php get_template_part('nav_category'); ?>
    </header>

    <article class="content content--archive">
      <!----------
        MAIN LOOP
      ------------>
      <?php
        // posts_per_page=-1:全件表示
        //query_posts($query_string.'&posts_per_page=-1');
      ?>
      <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
        <section class="content__post">

          <?php if ($wp_query->current_post < 2) : /*１，2番目の記事(レイアウト上の一番上)はスタイルシートを変える*/ ?>
            <figure class="content__post__figure content__post__figure--above">
          <?php else : ?>
            <figure class="content__post__figure">
          <?php endif; ?>

            <a href="<?php the_permalink(); ?>" class="content__post__figure__link">
              <?php the_post_thumbnail( 'full', array( 'class' => 'content__post__figure__eyecatch' ) ); ?>
              <div class="content__post__figure__description">
                <div class="content__post__figure__description__inside">
                  <h2 class="content__post__figure__description__title"><?php the_title(); ?></h2>
                  <figcaption class="content__post__figure__description__caption">
                    <?php
                      // the_tagsではリンクがついてしまい、aタグの中にaタグができてしまうので、独自にループ。
                      $tags = wp_get_post_terms( get_the_ID() , 'post_tag' , 'fields=names' );
                      if ($tags) {
                        echo implode(', ' , $tags);
                      }
                    ?>
                  </figcaption>
                </div>
              </div>
            </a>
          </figure>
        </section>
      <?php endwhile; endif;?>
    </article>

    <!-------
      FOOTER
    --------->
    <?php get_footer(); ?>

  </body>
</html>
