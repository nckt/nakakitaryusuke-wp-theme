<?php
/**
 * footer.php
 *
 * フッターです。
 *
 * @author junsuke nakakita <galikuson@gmail.com>
 */
?>
<footer class="footer">
  <address class="address">
    <div>
      <a href="https://www.instagram.com/ryusuke_nakakita" target="_blank"><img src="<?php bloginfo('template_directory');?>/static/img/instagram.png" alt="my instagram page" class="address__sns__image"></a>
      <a href="https://twitter.com/naca_lab" target="_blank"><img src="<?php bloginfo('template_directory');?>/static/img/twitter.png" alt="my twitter page" class="address__sns__image"></a>
      <a href="https://jp.pinterest.com/naca_lab/" class="content__post__figure__link" target="_blank"><img src="<?php bloginfo('template_directory');?>/static/img/pinterest.png" alt="my pinterest page" class="address__sns__image"></a>
      <a href="mailto:info@nakakitadesign.com" class="content__post__figure__link"><img src="<?php bloginfo('template_directory');?>/static/img/mail.png" alt="contact with mail" class="address__sns__image"></a>
    </div>
    <div class="address__mail">
      &copy; 2020 NAKAKITA DESIGN all rights reserved.
    </div>
  </address>
  <div class="up">
    <?php if ( get_post( get_the_ID() )->post_name != "about" ) : ?>
      <!-- ABOUTページではこれは出さない -->
      <a href="#" class="up__link"><img src="<?php bloginfo('template_directory');?>/static/img/up.gif" alt="ページの先頭へ戻る" class="up__img"></a>
    <?php endif; ?>
  </div>
</footer>
