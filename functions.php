<?php
  /**
   * functions.php
   *
   * @author junsuke nakakita <galikuson@gmail.com>
   */
// サムネイル有効化
add_theme_support( 'post-thumbnails' );

add_filter( 'wp_calculate_image_srcset_meta', '__return_null' );

/**
 * topimagesページに貼ってある画像をランダムで1枚取得
 */
function ryu__the_top_image() {
  // ページIDを取得
  $topimages_pagedata = get_page_by_path('top');

  // 画像IDを取得
  $images = array_keys(
    get_children(
      array(
        'post_parent'    => $topimages_pagedata->ID,
        'post_type'      => 'attachment',
        'post_mime_type' => 'image'
      )
    )
  );

  // 表示する画像IDを決定
  $image_count = count($images);
  $display_image_id = $images[ mt_rand(0, $image_count-1) ];

  // imgタグを表示
  print wp_get_attachment_image($display_image_id, 'full');
}

/**
 * mediaのカテゴリIDを返却。
 * @return id カンマ区切りのStringで
 */
 function ryu__get_media_ids() {
   $media_ids = array();
   $media = get_category_by_slug("publication");
   if ( is_object($media) ) {
     $media_ids[] = $media->term_id;
   }

   return implode(",", $media_ids);
 }
