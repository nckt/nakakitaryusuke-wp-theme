<?php
/**
 * head_elements.php
 *
 * 全ページ共通のheadタグ内の項目です。
 *
 * @author junsuke nakakita <galikuson@gmail.com>
 */
?>
<meta charset="utf-8" />
<title><?php wp_title(" | ", true, "right"); bloginfo('name'); ?></title>
<link rel="stylesheet" href="<?php bloginfo('template_directory');?>/static/css/out/styles.css" />
<meta name="viewport" content="width=device-width" />
<script type="text/javascript" charset="utf-8" src="<?php bloginfo('template_directory');?>/static/vendor/jquery/jquery-3.0.0.min.js"></script>
<script type="text/javascript" charset="utf-8" src="<?php bloginfo('template_directory');?>/static/js/common.js"></script>
<link rel="shortcut icon" href="<?php bloginfo('template_directory');?>/static/img/icon/favicon.ico" />
<meta name="keywords" content="中北隆介, nakakita ryusuke, CI, VI" />
<meta property="og:site_name" content="<?php bloginfo('name');?>" />
<meta property="og:locale" content="ja_JP" />
<meta property="og:title" content="<?php wp_title(" | ", true, "right"); bloginfo('name'); ?>" />
<script>
  (function(d) {
    var config = {
      kitId: 'rpi3iiq',
      scriptTimeout: 3000,
      async: true
    },
    h=d.documentElement,t=setTimeout(function(){h.className=h.className.replace(/\bwf-loading\b/g,"")+" wf-inactive";},config.scriptTimeout),tk=d.createElement("script"),f=false,s=d.getElementsByTagName("script")[0],a;h.className+=" wf-loading";tk.src='https://use.typekit.net/'+config.kitId+'.js';tk.async=true;tk.onload=tk.onreadystatechange=function(){a=this.readyState;if(f||a&&a!="complete"&&a!="loaded")return;f=true;clearTimeout(t);try{Typekit.load(config)}catch(e){}};s.parentNode.insertBefore(tk,s)
  })(document);
</script>
