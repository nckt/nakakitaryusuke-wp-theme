<html>
  <head prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb# article: http://ogp.me/ns/article#">
    <?php get_template_part('head_elements'); ?>

    <!----
      OGP
    ----->
    <meta property="og:type" content="website" />
    <meta property="og:image" content="<?php bloginfo('template_directory');?>/static/img/ryusukenakakita.png" />
    <meta property="og:description" content="I'm Ryusuke Nakakita, A Graphic designer based in Tokyo. This website is Mainly a limited display of my projects." />
    <meta name="twitter:card" content="summary" />
  </head>
<meta name="description" content=" 中北隆介(アートディレクター/グラフィックデザイナー)のウェブサイト。ブランドづくりの視点を軸に、ロゴ、パッケージ、広告、エディトリアル、ウェブサイト、トータルブランディングまで幅広く活動。" />
  <body>
    <!------------------
      PAGE TOP CONTENTS
    -------------------->
    <?php get_template_part('pagetop_contents'); ?>

    <!-------
      HEADER
    --------->
    <header class="header">
      <div>
        <p class="header__description">
        </p>
      </div>
    </header>

    <article class="content">
      <!--------------
        MAIN CONTENT
      --------------->
      <?php query_posts('posts_per_page=1'); ?>
      <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
        <section class="topimage">
          <?php /*the_post_thumbnail( 'full', array( 'class' => 'content__post__figure__eyecatch' ) );*/
            ryu__the_top_image()
          ?>
        </section>
      <?php endwhile; endif;?>
    </article>
  </body>
</html>
