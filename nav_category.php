<?php
/**
 * pagetop_contents.php
 *
 * ナビ（カテゴリ一覧）です。
 *
 * @author junsuke nakakita <galikuson@gmail.com>
 */
?>
<nav class="nav--detail">
  <ul class="nav--detail__list">
    <?php
    // publicationページの場合はナビ不要
    $page = get_post( get_the_ID() );
    if ($page->post_name != "publication") {
      $all_page_path = "/works/";
      $category_search_conditions = array();
      $category_search_conditions["orderby"]    = "id";
      $category_search_conditions["hide_empty"] = 0;

      $media_id = ryu__get_media_ids();
      $category_search_conditions["exclude"] = "1,${media_id}";
      echo '<li class="nav--detail__list__item">';
      echo '  <a href="' . $all_page_path . '" class="nav--detail__list__item__link">all</a>';
      echo '</li>';

      //------------------
      // カテゴリでループ
      //------------------
      foreach(
        get_categories($category_search_conditions) as $category
      ){
        echo '<li class="nav--detail__list__item">';
        echo '  <a href="' . get_category_link($category->term_id) . '" class="nav--detail__list__item__link">' . $category->name , '</a>';
        echo '</li>';
      }
    }
    ?>
  </ul>
</nav>
