<?php
/**
 * pagetop_contents.php
 *
 * ページ上部でスクロール追従してくる項目です。
 *
 * @author junsuke nakakita <galikuson@gmail.com>
 */
?>
<div class="logo">
  <?php /*** 以下は新デザイン公開時に有効化
  <?php if ( is_front_page() ) : ?>
    <!-- トップページのロゴ -->
    <a href="/" class="content__post__figure__link"><img src="<?php bloginfo('template_directory');?>/static/img/top_PC.png" alt="トップページに戻る" class="logo__img logo__img--PC logo__img--top" /></a>
    <a href="/" class="content__post__figure__link"><img src="<?php bloginfo('template_directory');?>/static/img/top_SP.png" alt="トップページに戻る" class="logo__img logo__img--SP logo__img--top" /></a>
  <?php else : ?>
    <!-- トップページ以外のロゴ -->
    <a href="/" class="content__post__figure__link"><img src="<?php bloginfo('template_directory');?>/static/img/logo.png" alt="トップページに戻る" class="logo__img logo__img--not_top" /></a>
  <?php endif; ?>
  ***/ ?>
  <a href="/" class="content__post__figure__link"><img src="<?php bloginfo('template_directory');?>/static/img/top.png" alt="トップページに戻る" class="logo__img logo__img--old" /></a>
</div>
<div class="menu">
  <div class="menu__icon">
    <img class="menu__icon__img menu__icon__img--open"  src="<?php bloginfo('template_directory');?>/static/img/menu.png"       alt="menu" />
    <img class="menu__icon__img menu__icon__img--close" src="<?php bloginfo('template_directory');?>/static/img/menu_close.png" alt="menu" />
  </div>

  <!-- PC用 -->
  <ul class="menu__list clearfix">
    <!-- works = topなので不要
    <li class="menu__list__item">
      <a href="/works/" class="menu__list__link">Works</a>
    </li>
    -->
    <li class="menu__list__item">
      <a href="/about/" class="menu__list__link">About</a>
    </li>
    <li class="menu__list__item">
      <a href="/publication/" class="menu__list__link">Publication</a>
    </li>
    <li class="menu__list__item">
      <a href="mailto:info@nakakitadesign.com" class="menu__list__link">Contact</a>
    </li>
  </ul>
</div>

<!-- スマホ用 -->
<div class="spmenu">
  <div class="spmenu__wrapper">
    <ul class="spmenu__list">
      <!-- works = topなので不要
      <li class="spmenu__list__item">
        <a href="/works/" class="spmenu__list__link">Works</a>
      </li>
      -->
      <li class="spmenu__list__item">
        <a href="/about/" class="spmenu__list__link">About</a>
      </li>
      <li class="spmenu__list__item">
        <a href="/publication/" class="spmenu__list__link">Publication</a>
      </li>
      <li class="spmenu__list__item">
        <a href="mailto:info@nakakitadesign.com" class="spmenu__list__link">Contact</a>
      </li>
    </ul>
  </div>
</div>
