<?php
/*
Template Name: publication
*/

/**
 * publication.php
 *
 * メディア一覧。media.phpから改名。
 *
 * @author junsuke nakakita <galikuson@gmail.com>
 */
query_posts(
  'orderby=post_date&'  .
  'order=DESC&'         .
  'post_type=post&'     .
  'post_status=publish&'.
  'posts_per_page=-1&'  .
  'category_name=publication'
);

get_template_part('archive');

wp_reset_query();
