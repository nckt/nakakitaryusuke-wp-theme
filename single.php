<html>
  <head prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb# article: http://ogp.me/ns/article#">
    <?php get_template_part('head_elements'); ?>

    <!----
      OGP
    ----->
    <meta property="og:type" content="article" />
    <meta name="twitter:card" content="summary_large_image" />
    <?php
      if ( have_posts() ) : while ( have_posts() ) : the_post();
        // サムネイル
        $eyeCatch;
        if ( has_post_thumbnail() ) {
          $imageID = get_post_thumbnail_id();
          $eyeCatch = wp_get_attachment_image_src($imageID, 'full')[0];
        } else {
          $eyeCatch = bloginfo('template_directory') . "/static/img/ryusukenakakita.png";
        }
        echo '<meta property="og:image" content="' . $eyeCatch . '" />';

        // 概要
        $description = strip_tags(
          get_post_meta(get_the_ID(), "description", true)
        );
        echo '<meta property="og:description" content="' . $description . '" />';
      endwhile; endif;
    ?>
  </head>
  <body class="single_body">
    <!------------------
      PAGE TOP CONTENTS
    -------------------->
    <?php get_template_part('pagetop_contents'); ?>

    <!-------
      HEADER
    --------->
    <header class="header">
      <!----
        NAV
      ------>
      <?php get_template_part('nav_category'); ?>
    </header>

    <!------------
      MAIN CONTENT
    -------------->
    <article class="content">
      <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
        <section class="post">
          <?php the_content(); ?>
          <h2><?php the_title(); ?></h2>
          <p class="category">
            <?php the_tags(''); ?>
          </p>
          <div class="post__description">
            <?php echo get_post_meta(get_the_ID(), "description", true); ?>
          </div>
          <div class="post__information">
            <?php
              $design = get_post_meta(get_the_ID(), "design", true);
              if( !empty($design) ){
                echo '<span class="post__information__item">Design:' . $design .'</span><br />';
              }

              $client = get_post_meta(get_the_ID(), "client", true);
              if( !empty($client) ){
                echo '<span class="post__information__item">Client:' . $client .'</span>';
              }
            ?>
          </div>
        </section>
      <?php endwhile; endif;?>
    </article>

    <article class="below_content below_content--detail">
      <a href="javascript:history.back();" class="below_content__link">BACK</a>
    </article>

    <!-------
      FOOTER
    --------->
    <?php get_footer(); ?>
  </body>
</html>
