/**
 * 記事一覧ページ固有のスクリプトです。
 */

/**
 * 画面描画完了後の処理
 */
$(function(){
  // アイキャッチにマウスオーバーするとページタイトルを表示する。
  displayWhenMouseOver(".content__post", ".content__post__figure__description", 200);
})
