/**
 * サイト全体で使うJSです。
 * @requires jQuery 3.0.0以上
 */

/**
 * マウスオーバーすると要素がふわって出てくるイベントを仕込みます。
 * @param mouseover マウスオーバーされるエレメントのセレクタ displayの親要素であること
 * @param display   表示されるエレメントのセレクタ mouseoverの子要素であること
 * @param duration  表示にかかる時間（ミリ秒）
 */
function displayWhenMouseOver(mouseover, display, duration){
  $(mouseover).mouseenter(function(){
    $(this).find(display).fadeIn(duration);
  });

  $(mouseover).mouseleave(function(){
    $(this).find(display).fadeOut(duration);
  });
}

/**
 * idが"googlemap"のエレメントに対してGoogle Mapを描画します。
 * ピンを立てたい座標を該当エレメントのdata-map-lat属性とdata-map-lng属性で指定してください。
 * 例：<div id="googlemap" data-map-lat="35.645173" data-map-lng="139.671259"></div>
 */
 var map;
 function initMap(){
   var mapDOM = document.getElementById('googlemap');
   var markerLat = Number(mapDOM.getAttribute("data-map-lat"));
   var markerLng = Number(mapDOM.getAttribute("data-map-lng"));
   map = new google.maps.Map(
     document.getElementById('googlemap'),
     {
       center : {lat: markerLat, lng: markerLng},
       zoom   : 18
     }
   );

   console.log(markerLat, markerLng);

   var marker = new google.maps.Marker({
     position : new google.maps.LatLng(markerLat, markerLng),
     map      : map
   });

   var mapStyle = [
     {
         "stylers": [
           { "saturation" : -100 },
           { "lightness"  :   -5 }
         ]
     }
   ];
   var mapType = new google.maps.StyledMapType(mapStyle);
   map.mapTypes.set('GrayScaleMap', mapType);
   map.setMapTypeId('GrayScaleMap');
 }

/**
 * 全ページ共通の描画完了後処理です。
 */
$(function(){
  // 右上のハンバーガーアイコンをクリックするとふわっとメニューが出てくる。
  $(".menu__icon").click(function(){
    $(this).find(".menu__icon__img--close").toggle();
    $(this).find(".menu__icon__img--open").toggle();
    $(".spmenu").fadeToggle(200);
  });

  // 右下の矢印をクリックするとページの先頭へ移動
  $(".up__link").click(function(){
    $("html,body").animate({scrollTop:0},"200");
  });

  // iOSでリンクの遷移に2タップ必要になる問題の対策
  // before要素のスタイル変更があるとそちらの描画が優先され、リンク遷移が起きないようだ。
  // style.lessの.textlink()でwidthを0->100%にしているので、それを無理矢理リセットする。あまりいい解決策ではないが。。妥協。
  if (('ontouchstart' in window) || window.DocumentTouch && document instanceof DocumentTouch) { // タッチデバイスか？
    $("head").append("<style>a:hover:before{width:0 !important;}</style>");
  }
})
