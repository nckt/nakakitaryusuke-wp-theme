<?php
/*
Template Name: works
*/

/**
 * works.php
 *
 * 全記事一覧。
 *
 * @author junsuke nakakita <galikuson@gmail.com>
 */
// mediaは除外する。
$media_id = ryu__get_media_ids();

query_posts(
  'orderby=post_date&'  .
  'order=DESC&'         .
  'post_type=post&'     .
  'post_status=publish&'.
  'posts_per_page=-1&'  .
  'category__not_in='.$media_id
);

get_template_part('archive');

wp_reset_query();
